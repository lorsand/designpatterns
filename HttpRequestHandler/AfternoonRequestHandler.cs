﻿using System.Net;
using System.Text;

public class AfternoonRequestHandler: HttpRequestHandler {
	
	public  const NAME = "/Afternoon";
	
	private ILogger logger;
	
	public AfternoonRequestHandler(ILogger logger)
	{
		this.logger = logger;
	}
	
	void Handle(HttpListenerContext context) {
		
		HttpListenerResponse response = context.Reponse;
		
		response.StatusCode = (int)HttpStatusCode.OK;
		
		string name = context.Request.QueryString["name"];
		
		string message;
		
		if(name == null)
		{
			message = "Good afternoon stranger";
		}
		else
		{
			message = "Good afternoon " + name;
		}
		
		byte[] messageBytes = Encoding.Default.GetBytes(message);
		
		serverResponse.OutputStream.Write(messageBytes, 0, messageBytes.Length);
		
		serverResponse.Close();
		
		logger.Log(context.Request.RawUrl);
	}
	
	string GetName() {
		return NAME;
	}
}