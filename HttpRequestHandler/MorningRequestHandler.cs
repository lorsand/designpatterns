﻿using System.Net;
using System.Text;

public class MorningRequestHandler: HttpRequestHandler {
	
	public  const NAME = "/Morning";
	
	private ILogger logger;
	
	public MorningRequestHandler(ILogger logger)
	{
		this.logger = logger;
	}
	
	void Handle(HttpListenerContext context) {
		
		HttpListenerResponse response = context.Reponse;
		
		response.StatusCode = (int)HttpStatusCode.OK;
		
		string name = context.Request.QueryString["name"];
		
		string message;
		
		if(name == null)
		{
			message = "Good morning stranger";
		}
		else
		{
			message = "Good morning " + name;
		}
		
		byte[] messageBytes = Encoding.Default.GetBytes(message);
		
		serverResponse.OutputStream.Write(messageBytes, 0, messageBytes.Length);
		
		serverResponse.Close();
		
		logger.Log(context.Request.RawUrl);
	}
	
	string GetName() {
		return NAME;
	}
}