﻿public interface HttpRequestHandler {
	void Handle(HttpListenerContext context);
	
	string GetName();
}