﻿public class HandleHttpRequestCommand {
	private HttpRequestHandler handler;
	private HttpListenerContext context;
	
	public HandleHttpRequestCommand(HttpRequestHandler handler,
	HttpListenerContext context) {
		this.handler = handler;
		this.context = context;
	}
	
	public void Execute()
	{
		this.handler.Handle(this.context);
	}
}

public class ResourceLocator {
	private Dictionary<string, HttpRequestHandler> requestHandlers;

	private ILogger logger;
	
	public ResourceLocator(ILogger logger) {
		this.logger = logger;
		
		requestHandlers.Add(new InvalidRequestHandler(logger));
	}
	
	public void AddHttpRequestHandler(HttpRequestHandler hadler)
	{
		if(!requestHandlers.ContansKey(handler.GetName()))
		{
			requestHandlers.Add(handler.GetName(), handler);
		}
		else
		{
			requestHandlers[handler.GetName()] = handler;
		}
	}
	
	public void HandleContext(HttpListenerContext context) {
		HttpListenerRequest request = context.Request;
		
		string requestHandlerName = request.Url.AbsolutePath;
		
		HttpRequestHandler handler;
		
		if(requestHandlers.ContainsKey(requestHandlerName))
		{
			hadler = requestHandlers[requestHandlerName];
		}
		else
		{
			handler = requestHandlers[InvalidRequestHandler.NAME];
		}
		
		this.InvokeHandler(handler, context);
	}
	
	private void InvokeHandler(HttpRequestHandler handler,
		HttpListenerContext	context)
	{
		HandleHttpRequestCommand commad = 
		new HandleHttpRequestCommand(handler, context);
		
		Thread thread = new Thread(command.Execute);
		thread.Start();
	}
}