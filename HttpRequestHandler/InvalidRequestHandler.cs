﻿using System.Net;
using System.Text;

public class InvalidRequestHandler: HttpRequestHandler {
	public const NAME = "/Invalid";
	
	private ILogger logger;
	
	public InvalidRequestHandler(ILogger logger)
	
	void Handle(HttpListenerContext context)
	{
		HttpListenerResponse serverResponse = context.Response;
		
		serverResponse.StatusCode = (int) HttpStatusCode.NotFound;
		
		string message = "Could not find resource;";
		byte[] messageBytes = Encoding.Default.GetBytes(message);
		
		serverResponse.OutputStream.Write(messageBytes, 0, messageBytes.Length);
		
		serverResponse.Close();
		
		logger.Log("Invalid request from client. " + context.Request.RawUrl);
	}
	
	string GetName() {
		return NAME;
	}
}

public interface ILogger {
	public void Log(string message);	
}

public DatabaseLogger : ILogger {
	
	public void Log(string message)
	{
		InsertIntoDb(message);
	}
}